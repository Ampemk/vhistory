## Author
Kwame Ampem

## Date
April 27 2017

## Synopsis

VehicleHistory.com
Coding Task


Reddit is a simple aggregator of social stories and web news content.  Further, it contains some additional features such up voting and discussions.


Task: Create a Reddit like web application. Create pages only for two categories (hot, new).


Features: Display the news feed.  The title should stand out and click to original article.  Pagination and simple search are required. No other functionality needed.


Scope: The application needs to have a backend API written in PHP and a frontend written in JavaScript (use your favorite framework.). If you can’t use a Frontend Framework, you can use a PHP framework, but all API calls to the PHP backend need to be made with JavaScript.


Extra Considerations: UI is of least concern.  You can use Bootstrap.
To use the reddit api, add .json at the end of the urls found on reddit’s site, ie: https://www.reddit.com/rising.json
  
You are free to use any framework / tools to build out the project.  The project is designed to take a few hours, it is not 100% necessary to complete all task.  Also, feel free to add more and show us your full range of skills.

## Motivation

My motivation for this project/task is to obtain a job with Vehicle History as a PHP Engineer

## Installation

Create an environmental variable file called .env at document root. (specify any database you want)
Connect local database or server database

.env should look like this
==============================================================

APP_ENV=local
APP_DEBUG=true
APP_KEY=H0MsOiLXsDYzNfhGp9O99d0ICisc3e0U - can be generated


DB_HOST=127.0.0.1
DB_DATABASE=VehicleHistory  - database of your choice
DB_USERNAME=[your username]
DB_PASSWORD=[your password]



CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

==============================================================




1. Run Composer Install / Composer Update
2. Run command [ php artisan migrate ] - migrates all of the database tables needed
3. Run command [ php artisan db:seed ] - seeds the database with Reddit data
  
You may need to run php artisan clear-compiled 



## API Reference

Uses reddit api to seed the database with interesting content. 

## Contributors

Not Necessary
