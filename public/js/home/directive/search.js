var vhistory = angular.module('vhistory');
vhistory.directive('search', function ($location) {
    return {
        restrict: 'A',
        scope: {
            term:'='
        },
        link: function (scope, element, attrs) {

            function functionToBeCalled () {

                //if we want to use a directive for the search
            }

            element.on('click', functionToBeCalled);
        }
    };
});