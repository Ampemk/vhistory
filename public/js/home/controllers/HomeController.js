var vhistory = angular.module('vhistory');

vhistory.controller('HomeController', ['$scope', '$routeParams', '$location', 'NewsFeed', function ($scope, $routeParams, $location, NewsFeed) {

        //Fetch the home news feed
        var category = ($routeParams.category || false);
        var nextpage = 1;
        var data = {}
        $scope.forward = true;


        //if category is not false then search by category
        if (category) {
            var data = {}
            data.searchTerm = category
            data.page = nextpage;

            NewsFeed.post(data).then(function (response) {

                $scope.newsfeed = response.data;

            })



        } else {
            var data = {}

            if ($location.path() == '/hot') {

                data.nav = 'hot';

                //fetch data based on nav tab (show)
                NewsFeed.show(data.nav).then(function (response) {

                    $scope.newsfeed = response.data;

                })
            } else if ($location.path() == '/new') {

                data.nav = 'new';
                
                //fetch data based on nav tab (show)
                NewsFeed.show(data.nav).then(function (response) {

                    $scope.newsfeed = response.data;

                })

            } else {
                
                data.nav = 'hot';
                
                //fetch data based on nav tab (show)
                NewsFeed.show(data.nav).then(function (response) {

                    $scope.newsfeed = response.data;

                })
            }
        }


        $scope.loadmore = function (direction) {
            //navigation 
            if (direction === 'forward') {
                nextpage = nextpage + 1;
                $scope.back = true;
                $scope.forward = true;

            } else if (direction === 'back') {
                nextpage = nextpage - 1;
                if (nextpage < 2) {
                    $scope.back = false;
                    $scope.forward = true;
                }
            }

            data.searchTerm = category;
            data.page = nextpage;

            NewsFeed.post(data).then(function (response) {

                $scope.newsfeed = response.data;
                
                if($scope.newsfeed.length < 10){
                    $scope.forward = false;
                    
                }else{
                    $scope.forward = true;
                    
                }

            })


        }


    }]);
