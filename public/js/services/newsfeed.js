angular.module('vhistory')
        .factory('NewsFeed', ['$resource', '$http', '$rootScope', function($resource,$http,$rootScope) {
                //$http.defaults.useXDomain = true;

                var feedApi = $resource("/feed/:tail", null, {
                    'update': {method: 'Put'}
                });      
                
                var feedPagination = $resource("/feed/:token", null, {
                    //'update': {method: 'Put'}
                });                      
                
                
                return {
                    
                    
                    get:function(){
                        
                        return feedApi.get().$promise.then(function(response){
                            
                            return response
                            
                        },function(error){
                            
                            return error
                        })
                    },
                    show:function(data){
                        
                        return feedApi.get({id:data}).$promise.then(function(response){
                            
                            return response
                            
                        },function(error){
                            
                            return error
                        })
                    },                    
                    post:function(data){
                        
                        return feedApi.save(data).$promise.then(function(response){
                            
                            return response
                            
                        },function(error){
                            
                            return error
                        })
                    },
                    delete:function(user){
                        return feedApi.delete({id:user}).$promise.then(function(response){
                            return response
                        },function(error){
                            return error
                        })
                    }
                    


                    
		
                }

}]);
