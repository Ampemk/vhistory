var vhistory = angular.module("vhistory", ['ngResource','ngRoute', 'ngSanitize','ui.bootstrap','ngAnimate']);
vhistory.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        //This is for the home page
        $routeProvider.when('/',{templateUrl: '/js/home/partials/home.html',controller:'HomeController'});
        $routeProvider.when('/search/:category',{templateUrl: '/js/home/partials/home.html',controller:'HomeController'});

        $routeProvider.when('/hot',{templateUrl: '/js/home/partials/home.html',controller:'HomeController'});
        $routeProvider.when('/new',{templateUrl: '/js/home/partials/home.html',controller:'HomeController'});

        // use the HTML5 History API
        $locationProvider.html5Mode(true);
    }])
