<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsFeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create the table for the news feed content
        Schema::create('news_feed',function($table){
            $table->increments('id');
            $table->string('title',255);
            $table->string('url',255);
            $table->string('thumbnail',255);
            $table->string('category',50);
            $table->integer('votes');            
            $table->integer('user_id');            
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //This drops the table
        Schema::dropTable('news_feed');
    }
}
