<?php

use Illuminate\Database\Seeder;
use App\NewsFeedModel;

class NewsFeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //get reddit hot data
        $this->parse_reddit('http://reddit.com/hot.json');
    }
    
    public function parse_reddit($filename){
        $reddit_data = file_get_contents($filename); // get output from database
        $reddit_json = json_decode($reddit_data); // decode it for objects
        
        
        foreach($reddit_json->data->children as $feed){

            //insert into database
            $newsfeedobj = new NewsFeedModel;
            
            $newsfeedobj->title=$feed->data->title;
            $newsfeedobj->url=$feed->data->url;
            $newsfeedobj->thumbnail = $feed->data->thumbnail;;
            $newsfeedobj->category = $feed->data->subreddit;
            $newsfeedobj->votes = rand(0, 100); // random votes
            $newsfeedobj->user_id = rand(0, 10); // random users
            $newsfeedobj->save();
            
           
        }
        
    }

}
