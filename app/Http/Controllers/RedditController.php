<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\NewsFeedModel;
use Illuminate\Support\Facades\DB;

class RedditController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //This gets the list of news feeds for us
        $hotfeed = NewsFeedModel::take(10)->get();

        return Response::json(['data' => $hotfeed->toArray()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //creates newsfeed if necessary
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //this stores the news feed but we will also use it for search
        if ($request->searchTerm) {
            $search = $request->searchTerm;
            $take = 10;
            if ($request->has('page')) {
                $page = $request->page;
                $skip = $take * ($page - 1);

                //fulltext search on title field - requires fulltext index on title column
                $data = DB::select(DB::raw("select f.title,f.thumbnail,f.url from news_feed f where match(f.title) against ('$search') and f.deleted_at is null Limit $take Offset $skip "));
            } else {
                //fulltext search on title field - requires fulltext index on title column
                $data = DB::select(DB::raw("select f.title,f.thumbnail,f.url from news_feed f where match(f.title) against ('$search') and f.deleted_at is null Limit $take"));
            }

            return Response::json(['data' => $data], 200); 
            
        } else if (!$request->searchTerm) {


            $take = 10;
            $page = $request->page;
            $skip = $take * ($page - 1);

            if ($request->has('nav')) {
                //navigation for per page search
                if ($request->nav === 'hot') {
                    $hot_mark = 10;
                    $data = NewsFeedModel::where('votes', '>', $hot_mark)->skip($skip)->orderby('votes', 'desc')->take(10)->get();
                    
                    return Response::json(['data' => $data->toArray()], 200);
                } else if ($request->nav === 'new') {
                    $data = NewsFeedModel::skip($skip)->orderby('created_at', 'desc')->take(10)->get();
                    return Response::json(['data' => $data->toArray()], 200);
                }
            }

            $data = NewsFeedModel::skip($skip)->take($take)->get();
            return Response::json(['data' => $data->toArray()], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //this shows the news feed to be used
        if ($id === 'hot') {
            $hot_mark = 10;
            $data = NewsFeedModel::where('votes', '>', $hot_mark)->orderby('votes', 'desc')->take(10)->get();
            return Response::json(['data' => $data->toArray()], 200);
        } else if ($id === 'new') {
            $data = NewsFeedModel::orderby('created_at', 'desc')->take(10)->get();
            return Response::json(['data' => $data->toArray()], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //This updates the news feed
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //This deletes the news feed
    }

}
